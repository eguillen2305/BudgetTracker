# BudgetTracker
 Basic Budget Tracking app can be used offline/online. 
 

## Summary

Progressive web application to help keep track of budget expenses and income.


![image](/image.jpg)


## How to use the Application

Access to Heroku:
https://budgettracker2305.herokuapp.com/


### Local Demo Install
1. first clone the application to your local file
`git clone`
2. install the packages with npm `npm install`
3. run the server with `node app.js`
4. run on local host on chrome "4000"

## Technology Used

#### Express
#### MLab MongoDB
#### MongoDB
#### Mongoose
#### Robo 3T

## Author
eguillen2035

## License
This project is licensed under the terms of the **MIT** license.

